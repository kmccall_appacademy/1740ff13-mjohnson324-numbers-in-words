class Fixnum
  def in_words
    return "zero" if self == 0
    number_string = to_s.reverse
    groupings = []
    first_digit = 0

    while number_string[first_digit]
      hundreds = number_string[first_digit..first_digit + 2].reverse
      words_to_add = Fixnum.words_to_999[hundreds.to_i]
      groupings.unshift(words_to_add)
      thousand_power = groupings.length - 1
      if groupings.first && first_digit > 2
        groupings[0] += " #{BIG_NUMBERS[1000**thousand_power]}"
      end
      first_digit += 3
    end
    groupings.compact.join(" ")
  end
  
  BIG_NUMBERS = {
    1000 => "thousand",
    1000000 => "million",
    1000000000 => "billion",
    1000000000000 => "trillion"
  }

  def self.words_to_999
    num_words = {
      1 => "one",
      2 => "two",
      3 => "three",
      4 => "four",
      5 => "five",
      6 => "six",
      7 => "seven",
      8 => "eight",
      9 => "nine",
      10 => "ten",
      11 => "eleven",
      12 => "twelve",
      13 => "thirteen",
      14 => "fourteen",
      15 => "fifteen",
      16 => "sixteen",
      17 => "seventeen",
      18 => "eighteen",
      19 => "nineteen",
      20 => "twenty",
      30 => "thirty",
      40 => "forty",
      50 => "fifty",
      60 => "sixty",
      70 => "seventy",
      80 => "eighty",
      90 => "ninety",
      100 => "one hundred",
      200 => "two hundred",
      300 => "three hundred",
      400 => "four hundred",
      500 => "five hundred",
      600 => "six hundred",
      700 => "seven hundred",
      800 => "eight hundred",
      900 => "nine hundred"
    }
    (21..999).each do |num|
      next if num_words.key?(num)
      if num.between?(21, 99)
        remain = num % 10
      else
        remain = num % 100
      end
      num_words[num] = num_words[num - remain] + " " + num_words[remain]
    end
    num_words
  end
end
